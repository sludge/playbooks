#!/bin/python36
#!/usr/bin/env python3
# install libsqlite
# python -m pip install requests PyChef pysqlite


in_awx = True

if in_awx:
  import sys
  orig_path = sys.path

  new_path = ['', '/usr/lib64/python36.zip', '/usr/lib64/python3.6', '/usr/lib64/python3.6/lib-dynload', '/usr/local/lib64/python3.6/site-packages', '/usr/local/lib/python3.6/site-packages', '/usr/lib64/python3.6/site-packages', '/usr/lib/python3.6/site-packages']

  sys.path = new_path

"""
tor-aip011:
  datacenter: tor   # opsplatform
  role: Aerospike   # opsplatform
  cluster: main     # opsplatform
  role_abbv: cit
  better_cluster: aip

tor-aip011 will appear in these groups:
- tor
- Aerospike
- main  (opsplatform shows this cluster as 'main')
- cit   (role abbv)
- aip

- tor-Aerospike
- tor-main
- tor-aip
- tor-cit

- tor-Aerospike-main
- tor-cit-aip

- tor-Aerospike-main-aip
----- and all the above with a suffix of '-active' (assuming tor-aip011 is active)
"""

import argparse
# import chef
import collections
import datetime
import json
import os
import pprint
import pickle
import requests
import subprocess
import textwrap
import yaml

pp = pprint.PrettyPrinter(depth=6)

examples = '''

TODO: rebuild when inventory file is missing
TODO: rebuild after an expiry period

Upon cloning, run:
  python ops-inventory.py --rebuild

This downloads opsplatform data, transforms it into inventory data, and stores it in a cache.

In addition to hosts, the inventory returns groups.

By default, group name patterns are datacenter-rolename, such as tor-Bidder and tor-Bidder-all. The former contains only nodes shown as active on opsplatform; the latter additionally contains inactive nodes.

Two parent groups are also created, one for datacenters and one for roles. For example, a `tor` group contains all groups in tor, such as tor-Bidder and tor-Vertica. A tor-all group contains tor-Bidder-all and tor-Vertica-all. Similarly for Bidder and Bidder-all. It contains tor-Bidder, lax-Bidder, and all other <datacenter>-Bidder groups.

Two flags used during --rebuild affect the names that are created.

--better-names: replaces '_' and ' ' with '-'.

--better-roles: replaces a number of role name with shorter names, such as Bidder becomes bid and Dataserver becomes data. See the role_abbvs declaration in the code. The Aerospike role becomes cit by default but also to aip/dedup/event/map/user if one of those is part of the node name. This allows playbooks to be targeted to a particular cluster, such as tor-aip.

These flags are recognized on the --rebuild command as well as the config file.


Examples:

# non-ansible calls:
  python ops-inventory.py --pretty | less
  python ops-inventory.py --list | less
  python ops-inventory.py --host tor-cit222
  python ops-inventory.py --stats

# ansible calls:

# a single node
  ansible -i ops-inventory.py tor-aip025 -m ping
# only active nodes in tor-Aerospike. Note: this works unless better-role is not set to True in the config file
  ansible -i ops-inventory.py tor-aip -m ping
# only active nodes in tor-aip. Note: this only works if better-role is set to True in the config file
  ansible -i ops-inventory.py tor-aip -m ping
# include inactive nodes
  ansible -i ops-inventory.py tor-aip-all -m ping
# active aip nodes in all datacenters
  ansible -i ops-inventory.py aip -m ping
# include inactive nodes
  ansible -i ops-inventory.py aip-all -m ping
# active tor nodes in all roles
  ansible -i ops-inventory.py tor -m ping
# include inactive nodes
  ansible -i ops-inventory.py tor-all -m ping
# options for aerospike nodes: cit, aip, dedup, event, map, user
'''


def source_path():
  """returns path of this file"""
  return '/'.join(os.path.realpath(__file__).split('/')[:-1])
def path():
  """returns path for finding inventory files"""
  return (source_path() + ':' + os.getcwd() + ':' + os.environ['HOME'] + ':/etc/ansible')
def find_in_path(filename, search_path, pathsep=os.pathsep):
  """ Given a search path, find file with requested name """
  for path in search_path.split(pathsep):
    candidate = os.path.join(path, filename)
    if os.path.exists(candidate):
      return os.path.abspath(candidate)
  return None
def find_inventory_file(filename=None):
  if filename:
    return filename
  else:
    return source_path() + '/.inventory.pickle'
def find_config_file(filename=None):
  if filename:
    return filename
  else:
    # return source_path() + '/.ops-inventory.conf.yaml'
    return find_in_path('.ops-inventory.conf.yaml', path(), ':')


class NodeBase:
  def __init__(self, better_names=False, better_roles=False):
    self.better_names = better_names
    self.better_roles = better_roles

  def reset_from_node(self, from_node):
    """ sets the node to the new raw data
    Typically it's better to create new objects each time, but
    this is more efficient (a pseudo flyweight design pattern)"""
    self.from_node = from_node
    self.to_node = None

  def normalize_name(self, s):
    if s == None: return None
    return s.replace('_', '-').replace(' ', '-')

  def short_name(self):
    name = self.from_node['Name']
    if self.better_names:
      first_token = name.split('.')[0]
      return self.normalize_name(first_token.replace('_', '-').replace(' ', '-')).lower()
    else:
      return name

  def scrunch(self, key):
    val = self.from_node[key]
    if isinstance(val, str): val = val.strip()
    if val == '': val = None
    return val

  def extract_from_hash(self, a_hash, attr_name):
    attr = self.scrunch(attr_name)
    if not self.better_roles: return attr
    val = a_hash.get(attr)
    if val:
      return val
    else:
      return attr

class OpsNode(NodeBase):
  from_clusters = {
    1: 'main'
  }

  role_abbvs = {
    # None: 'unknown',
    # '': 'unknown',
    'AdSquare': 'adsq',
    'Aerospike': 'cit',
    'aerospike-coldstorage': 'cit',
    'Bidder': 'bid',
    'Bidder-SHA': 'bidsha',
    'BudgetServer': 'budget',
    'Dataserver': 'data',
    'DeskUI': 'deskui',
    'Domain Controller': 'domain',
    'Factual': 'fact',
    'ProveDB': 'provdb',
    'SQSProcessor': 'sqs',
    'TaskService': 'task',
    'ToolsTwo': 'tool2',
    'Tracking': 'track',
    'Vertica': 'vert',
  }

  def __init__(self, from_node, better_names=False, better_roles=False):
    self.reset_from_node(from_node)
    super().__init__(better_names, better_roles)

  def node(self):
    if self.to_node == None: self.to_node = self.build_node()
    return self.to_node

  def better_cluster_name(self, nodename, rolename, clustername):
    if 'aip' in nodename: clustername = 'aip'
    if 'dedup' in nodename: clustername = 'dedup'
    if 'citevent' in nodename: clustername = 'event'
    if 'citmap' in nodename: clustername = 'map'
    if 'cituser' in nodename: clustername = 'user'
    if 'cold' in nodename: clustername = 'cold'
    return clustername

  def build_node(self):
    nodename = self.short_name()
    cluster_num = self.scrunch('Cluster')
    clustername = self.extract_from_hash(self.from_clusters, 'Cluster')
    if isinstance(clustername, int):
      clustername = "cluster%s" % (clustername)

    rolename = self.scrunch('RoleName')
    role_abbv = self.role_abbvs.get(rolename, None)
    better_clustername = self.better_cluster_name(nodename, rolename, clustername)

    internalip = self.scrunch('InternalIP')
    externalip = self.scrunch('ManagementIP')
    return {
      'name': self.short_name(),
      'datacenter-name': self.scrunch('Datacenter'),
      'rolename': rolename,
      'role_abbv': role_abbv,
      # 'better_rolename': better_rolename,
      'clustername': clustername,
      'better_clustername': better_clustername,
      'internalip': internalip,
      'externalip': externalip,
      'ip': externalip or internalip,
      'is-active': self.scrunch('StateDescription') == 'Active',
      'hostname': self.scrunch('Hostname'),
      'extra': {
        'cluster-id': cluster_num,
        'state-id': self.scrunch('State'),
        'state-name': self.scrunch('StateDescription'),
       }
     }


class AnsibleInventory:
  def __init__(self, nodes):
    self._nodes = nodes

  def add_to_hosts(self, group_dict, groupname, name_to_add, vars_dict = None):
    group = group_dict.get(groupname, None)
    if group == None:
      group = {"children": set(), "hosts": set(), "vars": {}}
      if vars_dict:
        group['vars'] = vars_dict
      group_dict[groupname] = group
    group['hosts'].add(name_to_add)

  def add_to_children(self, group_dict, groupname, child_groupname, vars_dict = None):
    group = group_dict.get(groupname, None)
    if group == None:
      group = {"children": set(), "hosts": set()}
      if vars_dict:
        group['vars'] = vars_dict
      group_dict[groupname] = group
    group['children'].add(child_groupname)

  def build(self):
    nodes = self._nodes
    hostvars = {}
    groups = {}
    better_groups = {}

    for node in nodes:
      nodename = node['name']
      datacenter = str(node['datacenter-name'])
      rolename = str(node['rolename'])
      role_abbv = str(node['role_abbv'])
      clustername = str(node['clustername'])
      better_clustername = str(node['better_clustername'])
      is_active = node['is-active']

      # hosts will go into either detailed_groupname or detailed_groupname+'-active'
      detailed_groupname = '%s-%s-%s-%s' % (datacenter, rolename, clustername, better_clustername)
      dc_role = '%s-%s' % (datacenter, rolename)
      dc_roleabbv = '%s-%s' % (datacenter, role_abbv)
      dc_cluster = '%s-%s' % (datacenter, clustername)
      dc_bettercluster = '%s-%s' % (datacenter, better_clustername)
      dc_role_cluster = '%s-%s-%s' % (datacenter, rolename, clustername)
      dc_roleabbv_bettercluster = '%s-%s-%s' % (datacenter, role_abbv, better_clustername)

      hostvars[nodename] = { 'ansible_host': node['ip'], 'datacenter': datacenter, 'role': rolename, 'cluster': clustername, 'role_abbv': role_abbv, 'better_cluster': better_clustername, 'is_active': is_active }
      group_hostvars = None

      active = ''
      self.add_to_hosts(groups, datacenter+active, nodename)
      self.add_to_hosts(groups, rolename+active, nodename)
      self.add_to_hosts(groups, clustername+active, nodename)
      self.add_to_hosts(groups, better_clustername+active, nodename)
      if role_abbv:
        self.add_to_hosts(groups, role_abbv+active, nodename)

      self.add_to_hosts(groups, dc_role+active, nodename)
      self.add_to_hosts(groups, dc_cluster+active, nodename)
      self.add_to_hosts(groups, dc_bettercluster+active, nodename)
      if role_abbv:
        self.add_to_hosts(groups, dc_roleabbv+active, nodename)

      self.add_to_hosts(groups, dc_role_cluster+active, nodename)
      if role_abbv:
        self.add_to_hosts(groups, dc_roleabbv_bettercluster+active, nodename)

      self.add_to_hosts(groups, detailed_groupname+active, nodename, group_hostvars)

      if node['is-active']:
        active = '-active'
        self.add_to_hosts(groups, datacenter+active, nodename)
        self.add_to_hosts(groups, rolename+active, nodename)
        self.add_to_hosts(groups, clustername+active, nodename)
        self.add_to_hosts(groups, better_clustername+active, nodename)
        if role_abbv:
          self.add_to_hosts(groups, role_abbv+active, nodename)

        self.add_to_hosts(groups, dc_role+active, nodename)
        self.add_to_hosts(groups, dc_cluster+active, nodename)
        self.add_to_hosts(groups, dc_bettercluster+active, nodename)
        if role_abbv:
          self.add_to_hosts(groups, dc_roleabbv+active, nodename)

        self.add_to_hosts(groups, dc_role_cluster+active, nodename)
        if role_abbv:
          self.add_to_hosts(groups, dc_roleabbv_bettercluster+active, nodename)

        self.add_to_hosts(groups, detailed_groupname+active, nodename, group_hostvars)

    return {'hostvars': hostvars, 'groups': groups, 'role_groups': better_groups}

  def to_str(self, data):
    hostvars = data['hostvars']
    groups = data['groups']
    role_groups = data['role_groups']

    all_children = {}

    for name, group_dict in groups.items():
      # d = { 'children': [] }
      d = {}
      all_children[name] = d
      if group_dict['children']:
        d['children'] = sorted(list(group_dict['children']))
      if group_dict['hosts']:
        d['hosts'] = sorted(list(group_dict['hosts']))
      if 'vars' in group_dict:
        # d['vars'] = sorted(list(group_dict['vars']))
        v = group_dict['vars']
        if v:
          d['vars'] = v

    for name, group_dict in role_groups.items():
      if not (name in all_children):
        d = { 'children': [] }
        all_children[name] = d
        if group_dict['children']:
          d['children'] = sorted(list(group_dict['children']))
        if group_dict['hosts']:
          d['hosts'] = sorted(list(group_dict['hosts']))
        if 'vars' in group_dict:
          # d['vars'] = sorted(list(group_dict['vars']))
          v = group_dict['vars']
          if v:
            d['vars'] = v

    all_children['_meta'] = { 'hostvars': hostvars }
    return all_children

  def add_parent_group(self, groups, parent_groupname, groupname):
    parent = groups.get(parent_groupname, {})
    children = set(parent.get('children', []))
    children.add(groupname)
    parent['children'] = list(children)
    groups[parent_groupname] = parent


class Opsplatform:
  def __init__(self, better_names, better_roles):
    self.ops_node = OpsNode(None, better_names, better_roles)

  def get_all_raw_nodes(self):
    key = os.environ.get('OPSPLATFORM_KEY', None)
    if not key:
      cmd = "aws ssm get-parameter --name opsplatform-api-key --with-decryption|awk '/Value/ {match($2,/([a-z0-9-]+)/,m); print m[1]}'"
      if in_awx: sys.path = orig_path
      result = subprocess.check_output(cmd, shell=True)
      if in_awx: sys.path = new_path
      key = str(result.decode("utf-8").strip())
    payload = { 'key': key }
    uri = 'https://opsplatform.adsrvr.org/api/Node'
    response = requests.get(uri, params=payload)
    json_resp = response.json()
    return json_resp

  def convert_node(self, raw_node):
    self.ops_node.reset_from_node(raw_node)
    return self.ops_node.node()

  def get_all_nodes(self):
    raw_nodes = self.get_all_raw_nodes()
    return [self.convert_node(raw_node) for raw_node in raw_nodes]






def show_stats(nodes):
  print('first five nodes:')
  pp.pprint(nodes[0:5])
  print()
  print(set([node.get('rolename') for node in nodes]))
  print()
  print('most common roles:')
  pp.pprint(collections.Counter([node.get('rolename') for node in nodes]).most_common(30))
  print()
  print('Number nodes: ', len(nodes))
  print('Number active nodes: ', len([node for node in nodes if node.get('is-active',False)]))

def get_inventory(better_names, better_roles):
  nodes = Opsplatform(better_names, better_roles).get_all_nodes()
  inventory_builder = AnsibleInventory(nodes)
  inventory_raw = inventory_builder.build()
  inventory = inventory_builder.to_str(inventory_raw)
  return inventory

def dump_inventory(filename, better_names, better_roles):
  nodes = Opsplatform(better_names, better_roles).get_all_nodes()
  inventory_builder = AnsibleInventory(nodes)
  inventory_raw = inventory_builder.build()
  inventory = inventory_builder.to_str(inventory_raw)
  data = {
    'created': datetime.datetime.now(),
    'inventory': inventory,
    'nodes': nodes,
  }
  outfile = open(find_inventory_file(filename),'wb')
  pickle.dump(data, outfile)
  outfile.close

def load_inventory(filename):
  infile = open(find_inventory_file(filename),'rb')
  data = pickle.load(infile)
  return data['nodes'], data['inventory']

def load_config_file(config_filename):
  try:
    with open(find_config_file(config_filename)) as f:
      return yaml.safe_load(f)
  except:
    {}


parser = argparse.ArgumentParser(
  description='Opsplatform Inventory',
  epilog=examples,
  formatter_class=argparse.RawDescriptionHelpFormatter
  )
parser.add_argument('--list', action='store_true', help='List all hosts, called by ansible')
parser.add_argument('--host', help='List specified host details, called by ansible')
parser.add_argument('--ip', help='Returns the ip address for the specified host')
parser.add_argument('--pretty', action='store_true', help='Like --host except pretty prints all hosts')
parser.add_argument('--rebuild', action='store_true', help='Rebuild cache')
parser.add_argument('--stats', action='store_true', help='Show stats')

parser.add_argument('--better-names', help='Replace underscores and spaces with hyphens, and keep only before the first period. Default is value in config file or False. Only relevant at rebuild time')
parser.add_argument('--better-roles', help='Improve role names (cit/map/user/aip... instead of Aerospike). Default is value in config file or False. Only relevant at rebuild time')
parser.add_argument('--config', help='Location of optional config file. Default is .ops-inventory.conf.yaml in the current directory, in the directory containing this file, in ~/, or /etc/ansible. See example.conf.yaml')
parser.add_argument('--inventory-filename', help='Location of the inventory cache. Default is .inventory.pickle')

args = parser.parse_args()
if args.better_names=='False': args.better_names=False
if args.better_roles=='False': args.better_roles=False


config_data = load_config_file(None)
if config_data:
  config_data = config_data.get('inventory', {})
else:
  config_data = {}

inventory_filename = args.inventory_filename or config_data.get('inventory-filename', None)
if args.better_names==None:
  better_names = config_data.get('better-names')
else:
  better_names = args.better_names
if args.better_names==None:
  better_roles = config_data.get('better-roles')
else:
  better_roles = args.better_roles

better_names = True
better_roles = True


# dump_inventory(inventory_filename, better_names, better_roles)
inventory = get_inventory(better_names, better_roles)

if args.host:
  # nodes, inventory = load_inventory(inventory_filename)
  host = inventory['_meta']['hostvars'].get(args.host, {})
  print(json.dumps(host, indent=2))
  # pp.pprint(host)

elif args.list:
  # nodes, inventory = load_inventory(inventory_filename)
  # print(inventory)
  print(json.dumps(inventory, indent=2))

elif args.ip:
  # nodes, inventory = load_inventory(inventory_filename)
  host = inventory['_meta']['hostvars'].get(args.ip, {})
  if host:
    print(host['ansible_host'])
  else:
    print('no such host')

elif args.pretty:
  # nodes, inventory = load_inventory(inventory_filename)
  print(json.dumps(inventory, indent=2))
  # pp.pprint(inventory)

elif args.rebuild:
  dump_inventory(inventory_filename, better_names, better_roles)

elif args.stats:
  nodes, inventory = load_inventory(inventory_filename)
  show_stats(nodes)
